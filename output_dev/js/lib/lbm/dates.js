define(["jquery", "datepicker"], (function( $ ) {
	$('input.date.time').datetimepicker({
		format: 'mm/dd/yyyy',
		autoclose: true,
	});

	$('input.date').datetimepicker({
		format: 'mm/dd/yyyy',
		autoclose: true,
		minView: 2,
	});

	$('input.date-range')
	.datetimepicker({
		format: 'm/d/yyyy',
		autoclose: true,
		minView: 2
	}).on('changeDate',function (e) {
		$( $(this).attr('data-target') ).datetimepicker('setStartDate', e.date);
	});

}));