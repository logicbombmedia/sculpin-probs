var require = {
    baseUrl: '/js',
    //urlArgs: "bust=" + (new Date()).getTime(),
    shim:  {
        ckeditorJquery : {
            deps: ['jquery', 'ckeditorCore']
        },
        datetimepicker : {
            deps: ['jquery'],
            exports: '$.fn.datetimepicker'
        }
    },
    paths: {
        // Library routes
        'jquery-ui':      'lib/jquery-ui/jquery-ui',
        jquery:           'lib/jquery',
        jqueryEasing:     'lib/jquery.easing.1.3',
        bootstrap:        'lib/bootstrap.min',
        modernizr:        'lib/modernizr-2.8.3.min',
        datepicker:       'lib/bootstrap-datetimepicker.min',
        modal:            'lib/lbm/modal',
        requests:         'lib/lbm/requests',
        dates:            'lib/lbm/dates',
        matchHeight:      'lib/matchHeight-min',

        // Devise routes
        devise:             '../packages/devisephp/cms/js',
        crossroads:         '../packages/devisephp/cms/js/lib/crossroads',
        signals:            '../packages/devisephp/cms/js/lib/signals',
        ckeditorCore:       '../packages/devisephp/cms/js/lib/ckeditor/ckeditor',
        ckeditorJquery:     '../packages/devisephp/cms/js/lib/ckeditor/adapters/jquery',
        spectrum:           '../packages/devisephp/cms/js/lib/spectrum',
        datetimepicker:     '../packages/devisephp/cms/js/lib/jquery.datetimepicker',
        moment:             '../packages/devisephp/cms/js/lib/moment',
        async:              '../packages/devisephp/cms/js/lib/millermedeiros-plugins/async',
        goog:               '../packages/devisephp/cms/js/lib/millermedeiros-plugins/goog',
        propertyParser :    '../packages/devisephp/cms/js/lib/millermedeiros-plugins/propertyParser',
        dvsEditor:          '../packages/devisephp/cms/js/app/devise',
        dvsPublic:          '../packages/devisephp/cms/js/app/public/preview',
        dvsInitialize:      '../packages/devisephp/cms/js/app/helpers/initialize-editor',
        dvsListeners:       '../packages/devisephp/cms/js/app/helpers/listeners',
        dvsNetwork:         '../packages/devisephp/cms/js/app/helpers/network',
        dvsNodeView:        '../packages/devisephp/cms/js/app/nodes/node-view',
        dvsFloaterSidebar:  '../packages/devisephp/cms/js/app/nodes/floaterNodeSidebar',
        dvsAdmin:           '../packages/devisephp/cms/js/app/admin/admin',
        dvsSidebarView:     '../packages/devisephp/cms/js/app/sidebar/sidebar-view',
        dvsCollectionsView: '../packages/devisephp/cms/js/app/sidebar/collections-view',
        dvsAdminView:       '../packages/devisephp/cms/js/app/admin/admin-view',
        dvsAdminPages:      '../packages/devisephp/cms/js/app/admin/pages',
        dvsLiveUpdate:      '../packages/devisephp/cms/js/app/helpers/live-update',
        dvsReplacement:     '../packages/devisephp/cms/js/app/helpers/replacement',
        dvsModal:           '../packages/devisephp/cms/js/app/helpers/modal',
        dvsGlobalBus:       '../packages/devisephp/cms/js/app/helpers/global-bus',
        dvsMessageBus:      '../packages/devisephp/cms/js/app/helpers/message-bus',
        dvsRouter:          '../packages/devisephp/cms/js/app/helpers/router',
        dvsRoutes:          '../packages/devisephp/cms/js/app/routes',
        dvsQueryHelper:     '../packages/devisephp/cms/js/app/helpers/query-helper',
        dvsPageData:        '../packages/devisephp/cms/js/app/helpers/page-data',
        dvsDatePicker:      '../packages/devisephp/cms/js/app/helpers/date-picker',
        dvsSelectSurrogate: '../packages/devisephp/cms/js/app/helpers/forms-select-surrogate'
    },
    devise: {
        partialLoaderPath:  '/admin/partials/'
    }
};