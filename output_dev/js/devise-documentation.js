$(function() {
	var windowHeight;
	var menuHeight;
	var menuWidth;
	var menuTop;
	var scrollPosition;
	var isSticky = false;

	function setWindowHeight() {

		windowHeight = $(window).height();

	}

	function setMenuHeight() {

		menuHeight = $('#documentation-sidebar').height();

	}

	function setMenuWidth() {

		menuWidth = $('#documentation-sidebar').width();

	}

	function setMenuTop() {

		var _offset = $('#documentation-sidebar').offset();
		console.log(_offset);
		menuTop = _offset.top;

	}

	function setScrollPosition() {

		scrollPosition = $(window).scrollTop();

	}

	function applySticky() {

		isSticky = true;

		var _classes = $('#documentation-sidebar').attr('class');
		var _shim = $('<div id="documentation-sidebar-shim">').addClass(_classes);

		_shim.removeClass('sticky');

		$('#documentation-sidebar').addClass('sticky').css('width', menuWidth);
		$('#documentation-sidebar').before(_shim);

	}

	function removeSticky() {

		isSticky = false;

		$('#documentation-sidebar').removeClass('sticky').css('width', null);
		$('#documentation-sidebar-shim').remove();

	}

	function calculateSidebarState() {

		if ((menuHeight + menuTop) - windowHeight < scrollPosition) {
			if (!isSticky) {
				applySticky();
			}
		} else {
			if (isSticky) {
				removeSticky();
			}
		}
	}

	function windowResizeListener() {
		$( window ).resize(function() {
			setWindowHeight();
		});
	}

	function windowScrollListener() {

		$(window).scroll(function (event) {
			setScrollPosition();
			calculateSidebarState();
		});
	}

	function init() {

		setWindowHeight();
		setMenuHeight();
		setMenuWidth();
		setScrollPosition();
		setMenuTop();

		windowResizeListener();
		windowScrollListener();

		calculateSidebarState();
	}

	init();
});